#ifndef __ENCODER_H
#define __ENCODER_H
#include "tim.h"
#include "ps2.h"
extern int V_A,V_B,V_C,V_D;
void CNT_init(void);
int Incremental_PID_calc_A(int spdw,int spdt);//spdw：期望速度，spdt:实际速度  单位：转每分钟
int Incremental_PID_calc_B(int spdw,int spdt);
int Incremental_PID_calc_C(int spdw,int spdt);
int Incremental_PID_calc_D(int spdw,int spdt);
void PWM_SET(char a,int pwm);
int WHEEL_SPD_UPD_B(void);
int WHEEL_SPD_UPD_A(void);
int WHEEL_SPD_UPD_C(void);
int WHEEL_SPD_UPD_D(void);
void AutoCtrl(uint8_t X_val,uint8_t Y_val,uint16_t key);
#endif
