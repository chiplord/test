#ifndef __PS2_H
#define __PS2_H

#include "main.h"
#include "cmsis_os.h"
#include "stm32f4xx_hal.h"
#include "gpio.h"
extern uint8_t Data[9];
extern uint16_t MASK[16];
extern uint16_t Handkey;
void delay_us(uint32_t udelay);
// uint16_t Handkey;	// 按键值读取，零时存储。
// uint8_t Comd[2] = { 0x01,0x42 };	//开始命令。请求数据
// uint8_t Data[9] = { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 }; //数据存储数组
 #define DI      HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0 );
#define PSB_SELECT      1
#define PSB_L3          2
#define PSB_R3          3
#define PSB_START       4
#define PSB_PAD_UP      5
#define PSB_PAD_RIGHT   6
#define PSB_PAD_DOWN    7
#define PSB_PAD_LEFT    8
#define PSB_L2          9
#define PSB_R2          10
#define PSB_L1          11
#define PSB_R1          12
#define PSB_GREEN       13
#define PSB_RED         14
#define PSB_BLUE        15
#define PSB_PINK        16

#define PSB_TRIANGLE    13
#define PSB_CIRCLE      14
#define PSB_CROSS       15
#define PSB_SQUARE      16
#define DO_H HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,GPIO_PIN_SET);        //命令位高
#define DO_L HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,GPIO_PIN_RESET);        //命令位低
 #define CS_H    HAL_GPIO_WritePin(GPIOC,GPIO_PIN_2,GPIO_PIN_SET);
#define CS_L    HAL_GPIO_WritePin(GPIOC,GPIO_PIN_2,GPIO_PIN_RESET);
#define CLK_H    HAL_GPIO_WritePin(GPIOC,GPIO_PIN_3,GPIO_PIN_SET);
#define CLK_L    HAL_GPIO_WritePin(GPIOC,GPIO_PIN_3,GPIO_PIN_RESET); 

//#define PS2_Cmd(cmd)     HAL_SPI_Transmit(&hspi1,&cmd,1,10); 
#define DELAY_TIME  delay_us(5); 


#define PSS_RX 5                //右摇杆X轴数据
#define PSS_RY 6
#define PSS_LX 7
#define PSS_LY 8
extern uint16_t MASK[16];
  void PS2_ClearData(void);
uint8_t PS2_DataKey(void);
void PS2_ReadData(void);
void PS2_SetInit(void);
void CNT_init(void);
uint8_t PS2_AnologData(uint8_t);
uint8_t PS2_RedLight(void);

#endif
