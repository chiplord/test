/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Encoder.h"
#include "PS2.h"
#include "stm32f4xx_it.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

	uint8_t	PS2_LX;    
  uint8_t PS2_LY;
	uint8_t PS2_RX;
	uint8_t PS2_RY;
	uint8_t PS2_KEY;	
	uint16_t angle1=1000,angle2=1000;
/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal4,
};
/* Definitions for Tsk_WHL_UPD */
osThreadId_t Tsk_WHL_UPDHandle;
const osThreadAttr_t Tsk_WHL_UPD_attributes = {
  .name = "Tsk_WHL_UPD",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityBelowNormal,
};
/* Definitions for PWM_GEN */
osThreadId_t PWM_GENHandle;
const osThreadAttr_t PWM_GEN_attributes = {
  .name = "PWM_GEN",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityBelowNormal6,
};
/* Definitions for Servo_control */
osThreadId_t Servo_controlHandle;
const osThreadAttr_t Servo_control_attributes = {
  .name = "Servo_control",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void StartTask02(void *argument);
void StartTask03(void *argument);
void StartTask04(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of Tsk_WHL_UPD */
  Tsk_WHL_UPDHandle = osThreadNew(StartTask02, NULL, &Tsk_WHL_UPD_attributes);

  /* creation of PWM_GEN */
  PWM_GENHandle = osThreadNew(StartTask03, NULL, &PWM_GEN_attributes);

  /* creation of Servo_control */
  Servo_controlHandle = osThreadNew(StartTask04, NULL, &Servo_control_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
	
	
  /* Infinite loop */
  for(;;)
  {
			PS2_LX=PS2_AnologData(PSS_LX);    
			PS2_LY=PS2_AnologData(PSS_LY);
			PS2_RX=PS2_AnologData(PSS_RX);
			PS2_RY=PS2_AnologData(PSS_RY);
			PS2_KEY=PS2_DataKey();	
if(PS2_KEY==PSB_L1)
		state=1;
	if(PS2_KEY==PSB_L2)
		state=0; 
if(PS2_KEY==PSB_R1)
		TIM1->CCR4=150;
    else
		TIM1->CCR4=100;
    
		osDelay(30);
  
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the Tsk_Msg_Trans thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void *argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */
  for(;;)
  { CNT_init();//重置计数器
    osDelay(40);//计数四十毫秒
		V_B=WHEEL_SPD_UPD_B();
		V_A=WHEEL_SPD_UPD_A();
		V_C=WHEEL_SPD_UPD_C();
		V_D=WHEEL_SPD_UPD_D();

  }
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_StartTask03 */
/**
* @brief Function implementing the PWM_GEN thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask03 */
void StartTask03(void *argument)
{
  /* USER CODE BEGIN StartTask03 */

  /* Infinite loop */
  for(;;)
{
	AutoCtrl(255-PS2_RX,PS2_RY,PS2_KEY)	;
//	int P_A=Incremental_PID_calc_A(60,V_A);//spdw：期望速度，spdt:实际速度  单位：转每分钟
//int P_B=Incremental_PID_calc_B(60,V_B);
//int P_C=Incremental_PID_calc_C(60,V_C);
//int P_D=Incremental_PID_calc_D(60,V_D);	
//	
//		PWM_SET('A',P_A);
//		PWM_SET('B',P_B);
//		PWM_SET('C',P_C);
//		PWM_SET('D',P_D);
    osDelay(10);
  }
  /* USER CODE END StartTask03 */
}

/* USER CODE BEGIN Header_StartTask04 */
/**
* @brief Function implementing the Servo_control thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask04 */
void StartTask04(void *argument)
{
  /* USER CODE BEGIN StartTask04 */
	TIM1->CCR1=1000;
	TIM1->CCR2=1000;
  /* Infinite loop */
  for(;;)
  {if(state==0)//0为手操
		{
		if(PS2_KEY==PSB_PAD_LEFT)
		AngleP++;
		if(PS2_KEY==PSB_PAD_RIGHT)
		AngleP--;}
		if(state==0)//0为手操
		{
		if(PS2_KEY==PSB_PAD_UP)
		AngleV++;
		if(PS2_KEY==PSB_PAD_DOWN)
		AngleV--;}
				if(AngleP>1070)
			AngleP=1070;
		if(AngleP<850)
			AngleP=850;
				if(AngleV>1000)
			AngleV=1000;
		if(AngleV<970)
			AngleV=970;
		if(angle1<AngleV)
		angle1+=1;
	else if(angle1>AngleV)
		angle1-=1;

	TIM1->CCR1=angle1;
	if(angle2<AngleP)
		angle2+=1;
	else if(angle2>AngleP)
		angle2-=1;
	TIM1->CCR3=angle2;
	TIM1->CCR1=angle1;
    osDelay(10);
  }
  /* USER CODE END StartTask04 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

