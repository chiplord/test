//#ifndef PS2_C
//#define PS2_C


#include "ps2.h"

 uint16_t Handkey;	// 按键值读取，零时存储。
 uint8_t Comd[2] = { 0x01,0x42 };	//开始命令。请求数据
 uint8_t Data[9] = { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 }; //数据存储数组
uint16_t MASK[16] = {
    PSB_SELECT,
    PSB_L3,
    PSB_R3 ,
    PSB_START,
    PSB_PAD_UP,
    PSB_PAD_RIGHT,
    PSB_PAD_DOWN,
    PSB_PAD_LEFT,
    PSB_L2,
    PSB_R2,
    PSB_L1,
    PSB_R1 ,
    PSB_GREEN,
    PSB_RED,
    PSB_BLUE,
    PSB_PINK
};
uint8_t PS2_RedLight(void)
{

	HAL_SPI_Transmit(&hspi1,&Comd[0],1,100);  //开始命令
	HAL_SPI_Transmit(&hspi1,&Comd[1],1,100); 
	HAL_SPI_Receive(&hspi1,&Data[1],1,100);
	if( Data[1] == 0X73)   return 0 ;
	else return 1;
}
void PS2_ReadData(void)
{
	volatile uint8_t byte=0;
	volatile uint16_t ref=0x01;
	HAL_SPI_Transmit(&hspi1,&Comd[0],1,100);  //开始命令
	HAL_SPI_Transmit(&hspi1,&Comd[1],1,100); 
	for(byte=2;byte<9;byte++)          //开始接受数据
	HAL_SPI_Receive(&hspi1,&Data[byte],1,1000);
}	
void PS2_ClearData(void)
{
	uint8_t a;
	for(a=0;a<9;a++)
		Data[a]=0x00;
}

//对读出来的PS2的数据进行处理,只处理按键部分  
//只有一个按键按下时按下为0， 未按下为1
uint8_t PS2_DataKey(void)
{
	uint8_t index;

	PS2_ClearData();
	PS2_ReadData();

	Handkey=(Data[4]<<8)|Data[3];     //这是16个按键  按下为0， 未按下为1
	for(index=0;index<16;index++)
	{	    
		if((Handkey&(1<<(MASK[index]-1)))==0)
		return index+1;
	}
	return 0;          //没有任何按键按下
}
//得到一个摇杆的模拟量	 范围0~256
uint8_t PS2_AnologData(uint8_t button)
{
	return Data[button];
}
	uint8_t a=0x01;
		uint8_t b=0x42,bcon=0x43,ana=0x44,lck=0x03;
	uint8_t c=0x00;
	uint8_t d=0x05A;
void PS2_ShortPoll(void)
{

HAL_SPI_Transmit(&hspi1,&a,1,100); 
  HAL_SPI_Transmit(&hspi1,&b,1,100); 
HAL_SPI_Transmit(&hspi1,&c,1,100); 
HAL_SPI_Transmit(&hspi1,&c,1,100); 
	HAL_SPI_Transmit(&hspi1,&c,1,100); 
	

}
void PS2_EnterConfing(void)
{
 
HAL_SPI_Transmit(&hspi1,&a,1,100); 
 HAL_SPI_Transmit(&hspi1,&bcon,1,100);
	HAL_SPI_Transmit(&hspi1,&c,1,100);
HAL_SPI_Transmit(&hspi1,&a,1,100); 
	HAL_SPI_Transmit(&hspi1,&c,1,100);
 	HAL_SPI_Transmit(&hspi1,&c,1,100); 
	HAL_SPI_Transmit(&hspi1,&c,1,100); 	
	HAL_SPI_Transmit(&hspi1,&c,1,100); 
	HAL_SPI_Transmit(&hspi1,&c,1,100); 

}
void PS2_ExitConfing(void)
{
HAL_SPI_Transmit(&hspi1,&a,1,100); 
  HAL_SPI_Transmit(&hspi1,&bcon,1,100); 
HAL_SPI_Transmit(&hspi1,&c,1,100); 
	HAL_SPI_Transmit(&hspi1,&c,1,100); 
	HAL_SPI_Transmit(&hspi1,&d,1,100); 
HAL_SPI_Transmit(&hspi1,&d,1,100); 
	HAL_SPI_Transmit(&hspi1,&d,1,100); 
HAL_SPI_Transmit(&hspi1,&d,1,100);
	HAL_SPI_Transmit(&hspi1,&d,1,100); 
}
void PS2_TurnOnAnalogMode(void){
HAL_SPI_Transmit(&hspi1,&a,1,100); 
  HAL_SPI_Transmit(&hspi1,&ana,1,100); 
HAL_SPI_Transmit(&hspi1,&a,1,100); 
	HAL_SPI_Transmit(&hspi1,&lck,1,100); 
	HAL_SPI_Transmit(&hspi1,&c,1,100); 
HAL_SPI_Transmit(&hspi1,&c,1,100); 
	HAL_SPI_Transmit(&hspi1,&c,1,100); 
HAL_SPI_Transmit(&hspi1,&c,1,100);
	HAL_SPI_Transmit(&hspi1,&c,1,100); 
}
void PS2_SetInit(void)
{
	PS2_ShortPoll();
	PS2_ShortPoll();
	PS2_ShortPoll();
	PS2_EnterConfing();		//进入配置模式
	PS2_TurnOnAnalogMode();	//“红绿灯”配置模式，并选择是否保存
	//PS2_VibrationMode();	//开启震动模式
	PS2_ExitConfing();		//完成并保存配置
}
//#endif