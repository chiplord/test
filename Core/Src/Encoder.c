#ifndef ENCODER_C
#define ENCODER_C

#include "encoder.h"
int A=0,B=0,C=0,D=0;
int V_A,V_B,V_C,V_D;
double kp=0.8, ki=0.6 , kd=0.2;
int Incremental_PID_calc_A(int spdw,int spdt){
	static int bias=0, last_bias=0, prev_bias=0; 
  bias=spdt-spdw;
  double P=kp*(bias-last_bias);
  double I=ki*bias;
	double D=kd*(bias-2*last_bias+prev_bias);
	prev_bias=last_bias;
	last_bias=bias;
	static int PID;PID-=(int)(P+I+D);
	if(PID>1000)
		PID=1000;
	if(PID<0)
		PID=0;
	return PID;
}
int Incremental_PID_calc_B(int spdw,int spdt){
	static int bias, last_bias, prev_bias; 
  bias=spdt-spdw;
  double P=kp*(bias-last_bias);
  double I=ki*bias;
	double D=kd*(bias-2*last_bias+prev_bias);
	prev_bias=last_bias;
	last_bias=bias;
	static int PID;PID-=(int)(P+I+D);
	if(PID>1000)
		PID=1000;
	if(PID<0)
		PID=0;
	return PID;
}
int Incremental_PID_calc_C(int spdw,int spdt){
	static int bias, last_bias, prev_bias; 
  bias=spdt-spdw;
  double P=kp*(bias-last_bias);
  double I=ki*bias;
	double D=kd*(bias-2*last_bias+prev_bias);
	prev_bias=last_bias;
	last_bias=bias;
	static int PID;PID-=(int)(P+I+D);
	if(PID>1000)
		PID=1000;
	if(PID<0)
		PID=0;
	return PID;
}
int Incremental_PID_calc_D(int spdw,int spdt){
	static int bias, last_bias, prev_bias; 
  bias=spdt-spdw;
  double P=kp*(bias-last_bias);
  double I=ki*bias;
	double D=kd*(bias-2*last_bias+prev_bias);
	prev_bias=last_bias;
	last_bias=bias;
	static int PID;PID-=(int)(P+I+D);
	if(PID>1000)
		PID=1000;
	if(PID<0)
		PID=0;
	return PID;
}                                                  //PID计算

void CNT_init(void){
__HAL_TIM_SetCounter(&htim3,10000);
	__HAL_TIM_SetCounter(&htim4,10000);
	__HAL_TIM_SetCounter(&htim2,10000);
	__HAL_TIM_SetCounter(&htim8,10000);
}//更新计数器

int WHEEL_SPD_UPD_A(void){
int Spd=(int)(int16_t)__HAL_TIM_GetCounter(&htim3)-10000;
	
return 60*25*Spd/1320;

}

int WHEEL_SPD_UPD_B(void){
int Spd=(int)(int16_t)__HAL_TIM_GetCounter(&htim4)-10000;
	
return 60*25*Spd/1320;

}
int WHEEL_SPD_UPD_C(void){
int Spd=(int)(int16_t)__HAL_TIM_GetCounter(&htim2)-10000;
	
return 60*25*Spd/1320;

}
int WHEEL_SPD_UPD_D(void){
int Spd=(int)(int16_t)__HAL_TIM_GetCounter(&htim8)-10000;
	
return 60*25*Spd/1320;

}

void PWM_SET(char a,int pwm){
	if(pwm<0)
		pwm=-pwm;
if(a=='A')
		__HAL_TIM_SET_COMPARE(&htim9,TIM_CHANNEL_1,pwm);
if(a=='B')
		__HAL_TIM_SET_COMPARE(&htim9,TIM_CHANNEL_2,pwm);
if(a=='C')
		__HAL_TIM_SET_COMPARE(&htim12,TIM_CHANNEL_1,pwm);
if(a=='D')
		__HAL_TIM_SET_COMPARE(&htim12,TIM_CHANNEL_2,pwm);
}

void AutoCtrl(uint8_t X_val,uint8_t Y_val,uint16_t key){
A=0,B=0,C=0,D=0;
	if(((X_val>=114)&&(X_val<=138))&&((Y_val>=114)&&(Y_val<=138))&&((key!=PSB_RED)&&(key!=PSB_PINK)))
	{	
		PWM_SET('A',0);
	PWM_SET('B',0);
		PWM_SET('C',0);
		PWM_SET('D',0);

	}//所有轮子刹车
	else{
	if(X_val>138)
	{A+=0.3*(X_val-138);
	D+=0.3*(X_val-138);
	C+=0.3*(114-X_val);
	B+=0.3*(114-X_val);}
	
	if(X_val<114)
	{A-=0.3*(114-X_val);
	D-=0.3*(114-X_val);
	B-=0.3*(X_val-138);
	C-=0.3*(X_val-138);}
	
	if(Y_val>138){
	A+=0.5*(Y_val-100);
	B+=0.5*(Y_val-100);
	C+=0.5*(Y_val-100);
	D+=0.5*(Y_val-100);
	}
	if(Y_val<114){
		A+=0.5*(Y_val-130);
	B+=0.5*(Y_val-130);
	C+=0.5*(Y_val-130);
	D+=0.5*(Y_val-130);
	}
	
	if(key==PSB_RED)
	{
		A+=50;
	  B-=50;
	C+=50;
	D-=50;
	}//右转
	if(key==PSB_PINK){
	A-=50;
	B+=50;
	C-=50;
	D+=50;
	}//左转

	//pwm输出
	if(A>=0)
	{HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET);
	PWM_SET('A',Incremental_PID_calc_A(A,V_A));}
	else
	{HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,GPIO_PIN_RESET);
	PWM_SET('A',Incremental_PID_calc_A(-A,-V_A));}
		if(B>=0)
	{HAL_GPIO_WritePin(GPIOD,GPIO_PIN_10,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_11,GPIO_PIN_RESET);PWM_SET('B',Incremental_PID_calc_B(B,V_B));}
	else
	{HAL_GPIO_WritePin(GPIOD,GPIO_PIN_11,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_10,GPIO_PIN_RESET);PWM_SET('B',Incremental_PID_calc_B(-B,-V_B));
	}
			if(C>=0)
	{HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_7,GPIO_PIN_RESET);	PWM_SET('C',Incremental_PID_calc_C(C,V_C));}
	else
	{HAL_GPIO_WritePin(GPIOD,GPIO_PIN_7,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_6,GPIO_PIN_RESET);	PWM_SET('C',Incremental_PID_calc_C(-C,-V_C));}
			if(D>=0)
	{HAL_GPIO_WritePin(GPIOD,GPIO_PIN_4,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_5,GPIO_PIN_RESET);	PWM_SET('D',Incremental_PID_calc_D(D,V_D));}
	else
	{HAL_GPIO_WritePin(GPIOD,GPIO_PIN_5,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_4,GPIO_PIN_RESET);	PWM_SET('D',Incremental_PID_calc_D(-D,-V_D));}
}

	
}

#endif
